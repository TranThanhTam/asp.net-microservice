﻿using static MangoStore.Utility.SD;

namespace MangoStore.Resquest
{
    public class ResquestDto
    {
        public ApiType Apitype { get; set; } = ApiType.GET;

        public string Url { get; set; }

        public string Data { get; set; }

        public string AccessToken { get; set; }
    }
}
