﻿using Mango.Services.MangoStore.ResponsModel;
using MangoStore.Resquest;
using MangoStore.Services.IServices;
using System.Net;
using System.Text;
using static MangoStore.Utility.SD;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MangoStore
{

	public class BaseSevices : IBaseServices
	{
		private readonly IHttpClientFactory _httpClientFactory;

		public BaseSevices(IHttpClientFactory httpClientFactory)
		{
			_httpClientFactory = httpClientFactory;
		}

		public async Task<ResponesDto?> SendAsync(ResquestDto resquestDto)
		
		{
			try
			{
				HttpClient http = _httpClientFactory.CreateClient("MangoAPI");
				HttpRequestMessage message = new();
				message.Headers.Add("Accept", "application/json");
				//TODO: Add Token


				message.RequestUri = new Uri(resquestDto.Url);
				if (resquestDto.Data != null)
				{
					message.Content = new StringContent(JsonSerializer.Serialize(resquestDto.Data), Encoding.UTF8, "application/json");
				}
				HttpResponseMessage? response = null;

				switch (resquestDto.Apitype)
				{
					case ApiType.POST:
						message.Method = HttpMethod.Post;
						break;
					case ApiType.PUT:
						message.Method = HttpMethod.Put;
						break;
					case ApiType.DELETE:
						message.Method = HttpMethod.Delete;
						break;
					default:
						message.Method = HttpMethod.Get;
						break;
				}
				response = await http.SendAsync(message);

				try
				{
					switch (response.StatusCode)
					{
						case HttpStatusCode.NotFound:
							return new()
							{
								isSuccess = false,
								message = "Not Found",
								code = "404"
							};
						case HttpStatusCode.Unauthorized:
							return new()
							{
								isSuccess = false,
								message = "Access Denied",
								code = "401"
							};
						case HttpStatusCode.Forbidden:
							return new()
							{
								isSuccess = false,
								message = "Forbidden",
								code = "403"
							};
						case HttpStatusCode.InternalServerError:
							return new()
							{
								isSuccess = false,
								message = "Internal Server Error",
								code = "500"
							};
						default:
							var result = await response.Content.ReadAsStringAsync();
							var apiRespones = JsonSerializer.Deserialize<ResponesDto>(result);
							return apiRespones;

					}
				}
				catch (Exception e)
				{
					var dto = new ResponesDto
					{
						isSuccess = false,
						message = e.Message,

					};
					return dto;
				}
			} catch (Exception e)
			{
				return new ResponesDto
				{
					isSuccess = false,
					message = e.Message
				};
			}
				
		}

		
	}
}
 