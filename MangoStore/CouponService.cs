﻿using Mango.Services.MangoStore.ResponsModel;
using MangoStore.Models;
using MangoStore.Resquest;
using MangoStore.Services.IServices;
using MangoStore.Utility;
using System.Text.Json;
using static MangoStore.Utility.SD;

namespace MangoStore
{
	public class CouponService : ICouponSevice
	{
		private readonly IBaseServices _baseServices;
		
		public CouponService(IBaseServices baseServices)
		{
			_baseServices = baseServices;
		}

		public string ApiBase { get; private set; }

		public async Task<ResponesDto?> CreateCoupon(CouponDto couponDto)
		{
			return await _baseServices.SendAsync(new ResquestDto
			{
				Url = SD.CouponAPIBase + "/api/Coupons/creatCoupon",
				Apitype = ApiType.POST,
				Data = JsonSerializer.Serialize(couponDto)
					
			});
		}

		public Task<ResponesDto?> DeleteCoupon(int couponId)
		{
			return _baseServices.SendAsync(new ResquestDto
			{
				Url = SD.CouponAPIBase + "api/Coupons" + couponId,
				Apitype = SD.ApiType.DELETE
			});
		}

		public async Task<ResponesDto?> GetAllCoupon()
		{

			return await _baseServices.SendAsync(new ResquestDto
			{
				Apitype = SD.ApiType.GET,
				Url = SD.CouponAPIBase + "api/Coupons"
			});
			
		}

		public async  Task<ResponesDto?> GetCouponById(int couponId)
		{
			return await _baseServices.SendAsync(new ResquestDto
			{
				Url = SD.CouponAPIBase + "/api/Coupons/" + couponId,
				Apitype = ApiType.GET
			});
		}

		public async  Task<ResponesDto?> UpdateCoupon(CouponDto couponDto)
		{
			return await _baseServices.SendAsync(new ResquestDto
			{
				Url = SD.CouponAPIBase + "/api/Coupons",
				Apitype = ApiType.PUT,
				Data = JsonSerializer.Serialize(couponDto)
			});
		}
	}
}
