﻿using MangoStore.Models;
using MangoStore.Services.IServices;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MangoStore.Controllers
{
	public class CouponController : Controller
	{
		private readonly ICouponSevice _couponSevice;
		public CouponController(ICouponSevice couponSevice)
		{
			_couponSevice = couponSevice;
		}

		public async Task<IActionResult> CouponIndex()
		{
			List<CouponDto> coupons = new();
			var response = await _couponSevice.GetAllCoupon();
			Console.WriteLine(response);
			if (response != null && response.isSuccess)
			{
				coupons = JsonSerializer.Deserialize<List<CouponDto>>(response.data.ToString() ?? string.Empty, new JsonSerializerOptions
				{
					PropertyNameCaseInsensitive = true
				});
			}
			return View(coupons);
		}

		public async Task<IActionResult> CreateCoupon()
		{
			CouponDto couponDto = new();
			return View(couponDto);			
		}
	}
}
