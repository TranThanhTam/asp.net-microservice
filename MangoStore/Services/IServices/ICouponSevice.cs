﻿using Mango.Services.MangoStore.ResponsModel;
using MangoStore.Models;

namespace MangoStore.Services.IServices
{
    public interface ICouponSevice
    {
        Task<ResponesDto?> CreateCoupon(CouponDto couponDto);
        Task<ResponesDto?> DeleteCoupon(int couponId);
        Task<ResponesDto?> GetAllCoupon();
        Task<ResponesDto?> GetCouponById(int couponId);
        Task<ResponesDto?> UpdateCoupon(CouponDto couponDto);

    }
}
