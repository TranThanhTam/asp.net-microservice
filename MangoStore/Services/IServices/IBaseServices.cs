﻿using Mango.Services.MangoStore.ResponsModel;
using MangoStore.Resquest;

namespace MangoStore.Services.IServices
{
	public interface IBaseServices
	{
		Task<ResponesDto?> SendAsync(ResquestDto resquestDto);
	}
}
