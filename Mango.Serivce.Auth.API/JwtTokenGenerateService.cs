﻿using Mango.Serivce.Auth.API.Models;
using Mango.Serivce.Auth.API.Services;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Mango.Serivce.Auth.API
{
	public class JwtTokenGenerateService : IJwtTokenGenerate
	{
		private readonly JwtOptions _jwtOptions;

		public JwtTokenGenerateService(IOptions<JwtOptions> jwtOptions)
		{
			_jwtOptions = jwtOptions.Value;
		}

		public string GenerateToken(AppUser appUser)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			byte[] keyBytes = Encoding.ASCII.GetBytes(_jwtOptions.Secert);
			var key = new SymmetricSecurityKey(keyBytes);

			var claimList = new List<Claim>
			{
				new Claim(JwtRegisteredClaimNames.Email, appUser.Email),
				new Claim(JwtRegisteredClaimNames.Sub, appUser.Id),
				new Claim(JwtRegisteredClaimNames.Name, appUser.UserName),
			};
			
			var descriptor = new SecurityTokenDescriptor
			{
				Audience = _jwtOptions.Audience,
				Issuer = _jwtOptions.Issuer,
				Subject = new ClaimsIdentity(claimList),
				Expires = DateTime.UtcNow.AddDays(7),
				SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
			};

			var token = tokenHandler.CreateToken(descriptor);

			return tokenHandler.WriteToken(token);
		}
	}
}
