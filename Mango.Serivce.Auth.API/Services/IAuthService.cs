﻿using Mango.Serivce.Auth.API.Models.Dto;
using Microsoft.AspNetCore.Identity;

namespace Mango.Serivce.Auth.API.Services
{
	public interface IAuthService
	{
		Task<string> Register(RegisterResquestDto resgesterResquestDto); 
		Task<LoginResponeDto> Login(LoginResquestDto loginResquestDto);
		Task<bool> AssignRole(string email, string roleName);

	}
}
