﻿using Mango.Serivce.Auth.API.Models;

namespace Mango.Serivce.Auth.API.Services
{
	public interface IJwtTokenGenerate
	{
		string GenerateToken(AppUser appUser);
	}
}
