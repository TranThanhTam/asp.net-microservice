﻿namespace Mango.Serivce.Auth.API.Models
{
	public class User
	{
		public string UserName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }

		public string FirstName { get; set; }
		public string LastName { get; set; }

		public string PhoneNumber { get; set; }
		public string Role { get; set; }
		public string AuthToken { get; set; }
		public string RefreshToken { get; set; }
		public DateTime? TokenExpiration { get; set; }
		public bool IsAuthSuccessful { get; set; }
		public IEnumerable<string> Roles { get; set; }
		public string ErrorMessage { get; set; }
		public string UserId { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string PostalCode { get; set; }	
		public string Name { get; set; }
	}
}
