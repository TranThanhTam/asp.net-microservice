﻿namespace Mango.Serivce.Auth.API.Models.Dto
{
	public class LoginResponeDto
	{
	    public string Token { get; set; }

		public AppUser User { get; set; }
	}
}
