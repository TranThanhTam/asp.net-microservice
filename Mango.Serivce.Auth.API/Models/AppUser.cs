﻿using Microsoft.AspNetCore.Identity;

namespace Mango.Serivce.Auth.API.Models
{
	public class AppUser : IdentityUser
	{
		public string Name { get; set; }

	}
}
