﻿
using Mango.Serivce.Auth.API.Models.Dto;
using Mango.Serivce.Auth.API.ResponsModel;
using Mango.Serivce.Auth.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace Mango.Serivce.Auth.API.Controllers
{
	[Route("api/auth")]
	[ApiController]
	public class AuthAPIController : ControllerBase
	{
		private readonly IAuthService _authService;
		protected ResponesDto _response;

		public AuthAPIController(IAuthService authService)
		{
			_authService = authService;
			_response = new ResponesDto();

		}
		[HttpPost("Resgeter")]
		public async Task<ActionResult> Resgeter([FromBody] RegisterResquestDto model)
		{
			var errorMesage = await _authService.Register(model);
			if (!string.IsNullOrEmpty(errorMesage))
			{
			    _response.isSuccess = false;
				_response.message = errorMesage;
				return BadRequest(_response);
			}
			return Ok(_response);
		}

		[HttpPost("Login")]
		public async Task<ActionResult> Login([FromBody] LoginResquestDto login)
		{
			var loginRespone = await _authService.Login(login);
			if (loginRespone == null)
			{
				_response.isSuccess = false;
				_response.message = "Login Failed";
				return BadRequest(_response);
			}
			_response.data = loginRespone;
			return Ok(_response);
		}

		[HttpPost("AssingRole")]
		public async Task<ActionResult> Role([FromBody] RegisterResquestDto model)
		{
			var assignRole = await _authService.AssignRole(model.Email, model.Role.ToLower());
			if (!assignRole)
			{
				_response.isSuccess = false;
				_response.message = "Login Failed";
				return BadRequest(_response);
			}
			return Ok(_response);
		}

	}
}
