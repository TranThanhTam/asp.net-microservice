using AutoMapper;
using Mango.Serivce.Auth.API;
using Mango.Serivce.Auth.API.Data;
using Mango.Serivce.Auth.API.Models;
using Mango.Serivce.Auth.API.Services;
using Mango.Services.CouponAPI.AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AppDbContext>(options =>
	options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

// add IMapper to the service container
IMapper mapper = MapperConfg.RegisterMappings().CreateMapper();
// create a mapper instance with singleton lifetime
builder.Services.AddSingleton(mapper);
// add automapper to the service container
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddIdentity<AppUser, IdentityRole>().AddEntityFrameworkStores<AppDbContext>().AddDefaultTokenProviders();
builder.Services.Configure<JwtOptions>(builder.Configuration.GetSection("ApiSettings:JwtOptions"));


builder.Services.AddScoped<IJwtTokenGenerate, JwtTokenGenerateService>();
builder.Services.AddScoped<IAuthService, AuthServices>();


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
ApplyMigrations();
app.Run();

void ApplyMigrations()
{
	using (var seviceScope = app.Services.CreateScope())
	{
		var _context = seviceScope.ServiceProvider.GetService<AppDbContext>();
		if (_context.Database.GetPendingMigrations().Count() > 0)
		{
			_context.Database.Migrate();
		}
	};
}
