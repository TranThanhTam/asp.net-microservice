﻿using Mango.Serivce.Auth.API.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.Contracts;

namespace Mango.Serivce.Auth.API.Data
{
	public class AppDbContext : IdentityDbContext<AppUser> { 
		public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
		
		public DbSet<AppUser> appUsers { get; set; } 
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}
