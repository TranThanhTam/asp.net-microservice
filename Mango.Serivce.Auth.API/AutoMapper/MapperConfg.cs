﻿using AutoMapper;
using Mango.Serivce.Auth.API.Models;
using Mango.Serivce.Auth.API.Models.Dto;


namespace Mango.Services.CouponAPI.AutoMapper
{
	public class MapperConfg
	{

		public  static MapperConfiguration RegisterMappings()
		{
			var mappingConfig = new global::AutoMapper.MapperConfiguration(config =>
			{
				config.CreateMap<ResponseDto, User>();
				config.CreateMap<User, ResponseDto>();
			});
			return mappingConfig;

		}
	}
}
