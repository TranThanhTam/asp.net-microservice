﻿using Mango.Serivce.Auth.API.Data;
using Mango.Serivce.Auth.API.Models;
using Mango.Serivce.Auth.API.Models.Dto;
using Mango.Serivce.Auth.API.Services;
using Microsoft.AspNetCore.Identity;

namespace Mango.Serivce.Auth.API
{
	public class AuthServices : IAuthService
	{
		private readonly AppDbContext _db;
		private readonly UserManager<AppUser> _userManager;
		private readonly RoleManager<IdentityRole> _roleManager;
		private readonly IJwtTokenGenerate _jwtTokenGenerate;

		public AuthServices( AppDbContext db, IJwtTokenGenerate jwtTokenGenerate
			,UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			_db = db;
			_jwtTokenGenerate = jwtTokenGenerate;
			_userManager = userManager;
			_roleManager = roleManager;
			
		}

		public async Task<bool> AssignRole(string email, string roleName)
		{
			var user = _db.appUsers.FirstOrDefault(u => u.Email == email);
			if(user == null)
			{
				return false;
			}
			else
			{
				_roleManager.CreateAsync(new IdentityRole(roleName)).GetAwaiter().GetResult();
			}
			await _userManager.AddToRoleAsync(user, roleName);
			return true;
		}

		public async Task<LoginResponeDto> Login(LoginResquestDto loginResquestDto)
		{
			var user = _db.appUsers.FirstOrDefault(u => u.UserName == loginResquestDto.UserName);
			bool checkPassword = await _userManager.CheckPasswordAsync( user, loginResquestDto.Password);

			if (user == null || checkPassword == false)
			{
				return new LoginResponeDto() { User = null, Token = "" };
			}
			var token = _jwtTokenGenerate.GenerateToken(user);

			AppUser userDto = new AppUser()
			{
				Id = user.Id,
				Email = user.Email,
				Name = user.Name,
				PhoneNumber = user.PhoneNumber,
				
			};
			
			LoginResponeDto loginResponeDto = new LoginResponeDto()
			{
				
				User = userDto,
				Token = token
			};
			return loginResponeDto;
		}

		public async Task<string> Register(RegisterResquestDto resgesterResquestDto)
		{
			AppUser user = new()
			{
				
				Email = resgesterResquestDto.Email,
				NormalizedEmail = resgesterResquestDto.Email.ToUpper(),
				UserName = resgesterResquestDto.Email,
				Name = resgesterResquestDto.Name,
				PhoneNumber = resgesterResquestDto.PhoneNumber,
				
				
			};

			try
			{
				var result = await _userManager.CreateAsync(user, resgesterResquestDto.Password);
                if (result.Succeeded)
                {
					var userToReturn = _db.appUsers.First(u => u.UserName == resgesterResquestDto.Email);
			         
						AppUser u = new()
						{
							Id = userToReturn.Id,
							Email = userToReturn.Email,
							Name = userToReturn.Name,
							PhoneNumber = userToReturn.PhoneNumber,
							
							
						};
					    return "";	
					} 
					else
					{
				         return result.Errors.FirstOrDefault().Description;
					}
                
            } catch(Exception e)
			{
			    
			}
			return "Error Creating User";
		}
	}
}
