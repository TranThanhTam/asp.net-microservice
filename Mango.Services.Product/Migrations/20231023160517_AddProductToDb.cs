﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Mango.Services.Product.Migrations
{
    /// <inheritdoc />
    public partial class AddProductToDb : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CategoryName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductId);
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "ProductId", "CategoryName", "CreatedDate", "Description", "ImageUrl", "Name", "Price", "UpdatedDate" },
                values: new object[,]
                {
                    { 1, "Smart Phone", new DateTime(2023, 10, 23, 23, 5, 17, 473, DateTimeKind.Local).AddTicks(9675), "IPhone X", "http://externalcatalogbaseurltobereplaced/api/pic/1", "IPhone X", 950.0, new DateTime(2023, 10, 23, 23, 5, 17, 473, DateTimeKind.Local).AddTicks(9686) },
                    { 2, "Smart Phone", new DateTime(2023, 10, 23, 23, 5, 17, 473, DateTimeKind.Local).AddTicks(9705), "IPhone 11", "http://externalcatalogbaseurltobereplaced/api/pic/2", "IPhone 11", 950.0, new DateTime(2023, 10, 23, 23, 5, 17, 473, DateTimeKind.Local).AddTicks(9706) }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
