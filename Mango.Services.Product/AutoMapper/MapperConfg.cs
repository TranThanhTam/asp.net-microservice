﻿using AutoMapper;
using Mango.Services.Product.Model;
using Mango.Services.Product.Model.Dto;

namespace Mango.Services.Product.AutoMapper
{
	public class MapperConfg
	{

		public  static MapperConfiguration RegisterMappings()
		{
			var mapperProduct = new global::AutoMapper.MapperConfiguration(config =>
			{
				config.CreateMap<ProductModel, ProductDto>();
				config.CreateMap<ProductDto, ProductModel>();
			});
			return mapperProduct;

		}
	}
}
