﻿using Mango.Services.Product.Model.Dto;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.Product.Data
{
	public class AppDataContext : DbContext
	{
		public AppDataContext(DbContextOptions<AppDataContext> options) : base(options)
		{
		}

		public DbSet<ProductDto> Products { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<ProductDto>().HasData(new ProductDto
			{
				ProductId = 1,
				Name = "IPhone X",
				Price = 950,
				Description = "IPhone X",
				CategoryName = "Smart Phone",
				ImageUrl = "http://externalcatalogbaseurltobereplaced/api/pic/1",
				CreatedDate = DateTime.Now,
				UpdatedDate = DateTime.Now
			});
			modelBuilder.Entity<ProductDto>().HasData(new ProductDto
			{
				ProductId = 2,
				Name = "IPhone 11",
				Price = 950,
				Description = "IPhone 11",
				CategoryName = "Smart Phone",
				ImageUrl = "http://externalcatalogbaseurltobereplaced/api/pic/2",
				CreatedDate = DateTime.Now,
				UpdatedDate = DateTime.Now
			});
		}
	}
}
