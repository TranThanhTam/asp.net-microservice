﻿using AutoMapper;
using Mango.Services.CouponAPI.AutoMapper;
using Mango.Services.CouponAPI.Data;
using Mango.Services.CouponAPI.Model;
using Mango.Services.CouponAPI.Model.Dto;
using Mango.Services.CouponAPI.ResponsModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace Mango.Services.CouponAPI.Controllers
{
	[Route("api/Coupons")]
	[ApiController]   

	public class CouponsController : ControllerBase
	{
		//add dependency injection
		private readonly AppDBContext _db;
		private ResponesDto respones;
		private IMapper _mapper;
		public CouponsController(AppDBContext db, IMapper mapper) 
		{
			_db = db;
			this.respones = new ResponesDto();
			_mapper = mapper;
		}


		[HttpGet]
		public object Get()
		{
			try {
				IEnumerable<Coupon> couponsList = _db.Coupons.ToList();
				respones.data = _mapper.Map<IEnumerable<CouponDto>>(couponsList);	
			}
			catch {
				respones.isSuccess = false;
				respones.message = "Internal Server Error";
			}
			return respones;
		}

		[HttpGet]
		[Route("{id:int}")]
		public ResponesDto Get(int id)
		{
			try
			{
				Coupon obj = _db.Coupons.FirstOrDefault(u => u.CouponId == id);
                if (obj != null)
                {
                    respones.data = _mapper.Map<Coupon>(obj);
                }

            }
			catch(Exception e)
			{
				respones.isSuccess = false;
				respones.message = e.ToString();
				return respones;

			}
			return respones;
		}
		[HttpPost]
		public ResponesDto Post ([FromBody] CouponDto couponDto)
		{
	       Coupon coupon = _mapper.Map<Coupon>(couponDto);
		   try {
				_db.Add(coupon);
				_db.SaveChanges();
		   }
			catch(Exception e) {
				respones.isSuccess = false;
				respones.message = e.Message;
			}
			return respones;
		}

		[HttpPut]
		public ResponesDto Put([FromBody] CouponDto couponDto)
		{
			Coupon coupon = _mapper.Map<Coupon>(couponDto);
			try
			{
				_db.Entry(coupon).State = EntityState.Modified;
				_db.SaveChanges();
			}
			catch (Exception e)
			{
				respones.isSuccess = false;
				respones.message = e.Message;
			}
			return respones;
		}
		[HttpDelete]
		[Route("{id:int}")]
		public ResponesDto Delete(int id)
		{
			try
			{
				Coupon obj = _db.Coupons.FirstOrDefault(u => u.CouponId == id);
				if (obj != null)
				{
					_db.Coupons.Remove(obj);
					_db.SaveChanges();
				}

			}
			catch (Exception e)
			{
				respones.isSuccess = false;
				respones.message = e.Message;
				return respones;

			}
			return respones;
		}
	}
}
