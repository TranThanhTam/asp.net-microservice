﻿using AutoMapper;
using Mango.Services.CouponAPI.Model;
using Mango.Services.CouponAPI.Model.Dto;

namespace Mango.Services.CouponAPI.AutoMapper
{
	public class MapperConfg
	{

		public  static MapperConfiguration RegisterMappings()
		{
			var mappingConfig = new global::AutoMapper.MapperConfiguration(config =>
			{
				config.CreateMap<CouponDto, Coupon>();
				config.CreateMap<Coupon, CouponDto>();
			});
			return mappingConfig;

		}
	}
}
