﻿using Mango.Services.CouponAPI.Model;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.CouponAPI.Data
{
	public class AppDBContext : DbContext
	{
		public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
		{

		}

		public DbSet<Coupon> Coupons { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.Entity<Coupon>().HasData(new Coupon
			{
				CouponId = 1,
				CouponCode = "10OFF",
				DiscountAmount = 10,
				MinAmount = 100
			});
			modelBuilder.Entity<Coupon>().HasData(new Coupon
			{
				CouponId = 2,
				CouponCode = "20OFF",
				DiscountAmount = 20,
				MinAmount = 200
			});
			modelBuilder.Entity<Coupon>().HasData(new Coupon
			{
				CouponId = 3,
				CouponCode = "30OFF",
				DiscountAmount = 30,
				MinAmount = 300
			});
			modelBuilder.Entity<Coupon>().HasData(new Coupon
			{
				CouponId = 4,
				CouponCode = "40OFF",
				DiscountAmount = 40,
				MinAmount = 400
			});
			modelBuilder.Entity<Coupon>().HasData(new Coupon
			{
				CouponId = 5,
				CouponCode = "50OFF",
				DiscountAmount = 50,
				MinAmount = 500
			});

			modelBuilder.Entity<Coupon>().HasData (new Coupon
			{
				CouponId = 6,
				CouponCode = "60OFF",
				DiscountAmount = 60,
				MinAmount = 600
			});
		}
	}
}
