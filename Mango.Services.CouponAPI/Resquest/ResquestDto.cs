﻿namespace Mango.Services.CouponAPI.Resquest
{
	public class ResquestDto
	{
		public string ApiType { get; set; } = "GET";

		public string Url { get; set; }

		public string Data { get; set; }

		public string AccessToken { get; set; }
	}
}
